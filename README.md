# Calulcating Greensapce Using NDVI

This project will explain how to calculate greenspace using NDVI in an urban environment.

## Purpose of this project:

The main goal of this project is to compare greenspace changes over time using Normalized Difference Vegetation Index (NDVI). The user will be able to calculate the NDVI using the formula on any desired number of 4 band rasters. Once the NDVI is calcaulted, the script will then reclassify values into three classes. The user will then be able to visually compare differences in rasters based on their NDVI class.

##### Format: Reusable Jupyter Notebook

## Data:

### Recommended raster type:

The rasters used for this analysis where NAIP images for 5 different years. The benefit to using NAIP is that images are available for nearly the whole country, and files are already formated exactly the same. 
NAIP imagery is not required but rasters must have four bands: RED, GREEN, Blue, and NAIP in that order.
A sample of rasters used are below. *Note a CIR NAIP Raster was used as the test data while creating the code. While this can be used in an analysis like this, the script here is not formated to except a CIR formated image where NIR is the first band, and red is the second band. 

![Sample Rasters](Images/2.jpg)

## For this analysis, set up the following things:

- Donwload your desired aerial image rasters. They must be four band images, with the NIR band as the fourth band. 
- Create a new folder called rasters in your workspace, and put ONLY the rasters in it.
- A shapefile for your area of interest that will be used to clip the rasters. Put this in your workspace folder but not the raster folder.
- Create an empty output folder.
Open the notebook and set your pathnames accordingly. Then run the code!

## Analysis:

What exactly is happening when NDVI is calculated? In essence, there are many parameters in the electromagnetic spectrum beyond the few we can see with our naked eye. Plants absorb visible light from the sun (mainly red), and re-emit various levels of Near Infrared light. Healthy vegetation will absorb lots of red light causing very little to be reflected back to the sensor, and also re-emit high levels of NIR. Compared to unhealthy plants or non absorbent surfaces like bare Earth or development, where not a whole lot of red light is being absorbed but only a slightly less amount of NIR is being captured by the sensor. So by using the NDVI formula we are essentially creating a ratio between how much visible light (red band) is being absorbed, and how much NIR (fourth band) is being reflected back to the sensor. The closer the NIR and Red band numbers are to each other the smaller the ratio, to the point where you can get a negative result. This is because buildings, concrete and other non-living surfaces do not reflect any NIR wavelengths, so we can discern these from plants, which shows us the greenspaces versus non greenspaces. This allows us to make calculated estimates on greenspace levels for any given area in the rasters. 

The image below shows how NDVI is calculated.
![NDVI](Images/4.jpg)
The reclass portion of the script was based on a similar study conducted at the Universiti Teknologi MARA in Malaysia, using three classes for urban environments.
![Reclass](Images/Screenshot%202021-04-27%20130622.png)

## Outputs:

The outputs for this analysis are as follows:
- Raster clipped to area of interest shapefile (TIFF)
- NDVI Raster (TIFF)
- Reclassified raster (TIFF)
- Shapefile displaying class two- low vegetation (shp)
- Shapefile displaying class three- high vegetation (shp)

The best way to view the outputs is to open the files in ArcPro and use layouts to compare images side by side. Below is an example of how to compare the outputs.

## Low Vegetation Greenspace 0.2 - 0.5
![Low Vegetation 0.2 - 0.5](Images/Layout1.png)

## High Vegetation Greenspace 0.5 - 1
![High Vegetation 0.5 - 1.0 ](Images/Layout2.png)
